package view;


import java.io.IOException;
import javax.swing.ImageIcon;
import model.CheckerBoardShadow.PiecesType;

/**
 * DamaBox interface
 */
public interface DamaBox {

    /**
     * this method associate the piece (WP,WD,BD,BP) with the image on the checkerBoard
     * @param {@link piecesType} type of the piece
     * @throws IOException 
     */
    void setPiece(PiecesType type) throws IOException;

    /**
     * this method resize the images to fit into the checkerBoard
     * @param {@link String} path of the images
     */
    void resize(ImageIcon icon);

}
