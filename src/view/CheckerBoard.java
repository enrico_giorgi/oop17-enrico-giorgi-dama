package view;

import java.io.IOException;

import model.CheckerBoardShadow.PiecesType;

/**
 * CheckerBoard interface
  */
public interface CheckerBoard {

    /**
     * set the piece on the checkerBoard
     * @param x {@link integer} - row of the checkerBoard
     * @param y {@link integer} - column of the checkerBoard
     * @param pt {@link PiecesType} - current piece to set on the checkerBoard 
     * @throws IOException 
     */
    void setPieces(int x, int y, PiecesType pt) throws IOException;

    /**
     * reset the Red border paths on all the checkerBoard
     */
    void resetRedBorder();

    /**
     * Set the red Border path (where i can move the piece) on position x,y
     * @param x {@link integer} - row
     * @param y {@link integer} - column
     */
    void setBorder(int x,int y);

    void close();
}
